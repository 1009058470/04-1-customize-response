package com.twuc.webApp.controllerTest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ResponseTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void test_has_no_returnValue() throws Exception {
        mockMvc.perform(get("/api/no-return-value"))
                .andExpect(status().isOk());  // maybe success  //return ok
    }

    @Test
    void test_no_return_value_with_annotation() throws Exception {
        mockMvc.perform(get("/api/no-return-value-with-annotation"))
                .andExpect(status().isNoContent());
    }
}
