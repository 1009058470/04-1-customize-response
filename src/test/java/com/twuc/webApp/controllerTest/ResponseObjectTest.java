package com.twuc.webApp.controllerTest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class ResponseObjectTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void test_message_entities() throws Exception {
        mockMvc.perform(get("/api/message-entities/123"))
                .andExpect(status().isOk())
                .andExpect(header().string("X-Auth","Me"))
                .andExpect(content().string("{\"value\":\"123\"}"));
    }
}
