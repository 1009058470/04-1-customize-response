package com.twuc.webApp.controller;

import com.twuc.webApp.Message.Message;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResponseEntityController {

    @GetMapping("/api/message-entities/{message}")  //
    public ResponseEntity<Message> messageEntities(@PathVariable Message message){
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Auth", "Me");

        return new ResponseEntity<Message>(
                message, headers, HttpStatus.OK);
    }
}
