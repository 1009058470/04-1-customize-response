package com.twuc.webApp.controller;

import com.twuc.webApp.Message.Message;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResponseAObject {
    @GetMapping("/api/message-objects/{message}")
    @ResponseStatus
     public ResponseEntity<Message> responseMessageObject(@PathVariable String message){
        return ResponseEntity.status(200).body(new Message(message));
    }
}
