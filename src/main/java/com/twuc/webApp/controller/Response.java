package com.twuc.webApp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Response {
    @GetMapping("/api/no-return-value")
    public ResponseEntity<String> notReturnValue(){
        return ResponseEntity.ok().body("no_retur_value");
    }


    @GetMapping("/api/no-return-value-with-annotation")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void no_return_value_with_annotation() {
        //return ResponseEntity.status(204).body("");
    }

}
