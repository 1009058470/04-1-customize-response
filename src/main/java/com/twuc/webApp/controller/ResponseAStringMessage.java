package com.twuc.webApp.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;

@RestController
public class ResponseAStringMessage {
    @GetMapping("/api/messages/{message}")
    public ResponseEntity<String> responseMessageAndStatus(@PathVariable("message")String message){
        return ResponseEntity.status(200).body(message);
    }
}
