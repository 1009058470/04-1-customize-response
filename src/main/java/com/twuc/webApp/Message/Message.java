package com.twuc.webApp.Message;

public class Message {
    public String getValue() {
        return value;
    }

    public Message(String value) {
        this.value = value;
    }

    public Message(){

    }

    public void setValue(String value) {
        this.value = value;
    }

    private String value;
}
